﻿using System.ComponentModel.DataAnnotations.Schema;

namespace InterviewCodeBridge.DAL.Entities
{
    public class Dog
    {
        
        public int Id { get; set; }
        [Column("name")]
        public string Name { get; set; }
        [Column("color")]
        public string Color { get; set; }
        [Column("tail_length")]
        public int TailLength { get; set; }
        [Column("weight")]
        public int Weight { get; set; }

    }
}
