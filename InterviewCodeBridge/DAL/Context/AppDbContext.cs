﻿using InterviewCodeBridge.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace InterviewCodeBridge.DAL.Context
{
    public class AppDbContext :DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {

        }
        public DbSet<Dog> Dogs { get; set; }
    }
}
