﻿using System;

namespace InterviewCodeBridge.Errors
{
    public class CustomError : Exception
    {
        public CustomError(string errorMessage)
            : base(errorMessage)
        {

        }
    }
}
