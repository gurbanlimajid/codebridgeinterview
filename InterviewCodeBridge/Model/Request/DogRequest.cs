﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace InterviewCodeBridge.Model.Request
{
    public class DogRequest
    {
        [Required]
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("color")]
        [Required]
        public string Color { get; set; }
        [Required]
        [JsonPropertyName("tail_length")]
        public int TailLength { get; set; }
        [Required]
        [JsonPropertyName("weight")]
        public int Weight { get; set; }
    }
}
