﻿namespace InterviewCodeBridge.Model.Response
{
    public class PingResponse
    {
        public string ResponseMessage { get; set; }
        public PingResponse(string responseMessage)
        {
            this.ResponseMessage = responseMessage;

        }
    }
}
