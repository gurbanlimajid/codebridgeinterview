﻿using InterviewCodeBridge.DAL.Entities;
using InterviewCodeBridge.Errors;
using InterviewCodeBridge.Model.Request;
using InterviewCodeBridge.Model.Response;
using InterviewCodeBridge.Repo;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InterviewCodeBridge.Service
{
    public class DogService : IDogService
    {
        private readonly IDogRepo dogRepo;

        public DogService(IDogRepo dogRepo)
        {
            this.dogRepo = dogRepo;
        }
        public async Task CreateDog(DogRequest dogRequest)
        {
            if (dogRequest.TailLength <= 0)
            {
                throw new CustomError("tail_length_error");

            }
            if(dogRequest.Weight <= 0)
            {
                throw new CustomError("weight_length_error");
            }
            await dogRepo.CheckExistance(dogRequest);
            Dog dog = new Dog()
            {
                Color = dogRequest.Color,
                Name = dogRequest.Name,
                TailLength = dogRequest.TailLength,
                Weight = dogRequest.Weight,
            };
            await dogRepo.CreateDog(dog);
        }
        public bool CheckInputValidation(string attribute, string order, int? pageNumber, int? pageSize)
        {
            if ((pageNumber != null && pageSize == null) || (pageNumber == null && pageSize != null))
            {
                throw new CustomError("number_size_error");
            }
            if ((attribute != null && order == null) || (attribute == null && order != null))
            {
                throw new CustomError("attribute_order_error");
            }
            if (pageNumber <= 0 || pageSize <= 0)
            {
                throw new CustomError("pagination_not_correct");
            }
            return true;

        }
        public async Task<List<DogResponse>> GetDogs(string attribute, string order, int? pageNumber, int? pageSize)
        {
            CheckInputValidation(attribute,order,pageNumber,pageSize);
            string orderBy = null;        
            try
            {
                if (attribute != null)
                {
                    attribute = Helper.Helper.Attribute[attribute];
                    var temp =order.ToUpper();
                    if (order.ToUpper() == "ASC" || order.ToUpper() == "DESC")
                    {
                        orderBy = attribute + " " + order.ToUpper();

                    }
                    else
                    {
                        throw new CustomError("order_not_correct");
                    }
                }

            }
            catch (System.Collections.Generic.KeyNotFoundException ex)
            {

                throw new CustomError("attribute_not_correct");
            }
            int? beginIndex = null ;
            if(pageNumber != null)
            {

                beginIndex = pageNumber * pageSize - pageSize;
            }
          var dogs=  await dogRepo.GetDogs(orderBy, beginIndex, pageSize);
            List<DogResponse> dogResponses = new List<DogResponse>();
            foreach (var dog in dogs)
            {
                var dogResponse = new DogResponse()
                {
                    Name=dog.Name,
                    Color=dog.Color,
                    TailLength=dog.TailLength,
                    Weight=dog.Weight
                };
                dogResponses.Add(dogResponse);

            }

           return dogResponses;

           
        }
    }
}
