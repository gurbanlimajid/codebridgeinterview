﻿using InterviewCodeBridge.DAL.Entities;
using InterviewCodeBridge.Model.Request;
using InterviewCodeBridge.Model.Response;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InterviewCodeBridge.Service
{
    public interface IDogService
    {
        public Task CreateDog(DogRequest dogRequest);
        public Task<List<DogResponse>> GetDogs(string attribute, string order, int? pageNumber, int? pageSize);
        public bool CheckInputValidation(string attribute, string order, int? pageNumber, int? pageSize);
    }
}
