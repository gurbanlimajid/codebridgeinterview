﻿using System.Collections.Generic;

namespace InterviewCodeBridge.Helper
{
    public static class Helper
    {
        public static Dictionary<string, string> Attribute = new Dictionary<string, string>()
        {
            {
                "name","Name"
            },
            {
                "color","Color"
            },
            {
                "tail_length","TailLength"
            },
            {
                "weight","Weight"
            }

        };
    }
}
