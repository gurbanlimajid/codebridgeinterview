﻿using InterviewCodeBridge.DAL.Context;
using InterviewCodeBridge.DAL.Entities;
using InterviewCodeBridge.Errors;
using InterviewCodeBridge.Model.Request;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using System.Collections.Generic;

namespace InterviewCodeBridge.Repo
{
    public class DogRepo : IDogRepo
    {
        private readonly AppDbContext appDbContext;

        public DogRepo(AppDbContext appDbContext)
        {
            this.appDbContext = appDbContext;
        }
        public async Task CheckExistance(DogRequest dogRequest)
        {
            var dog = await appDbContext.Dogs.FirstOrDefaultAsync(x=>x.Name==dogRequest.Name);
            if (dog != null)
                throw new CustomError("dog_duplicate_error");
        }

        public async Task CreateDog(Dog dog)
        {
            await appDbContext.Dogs.AddAsync(dog);
            var dbContextResult =  await appDbContext.SaveChangesAsync();
            if (dbContextResult <= 0)
            {
                throw new CustomError("create_error");
            }
        }

        public async Task<List<Dog>> GetDogs(string orderBy, int? beginIndex, int? pageSize)
        {
            List<Dog> dogs = null;
            if (orderBy != null&&beginIndex!=null)
            {
                dogs = await appDbContext.Dogs.OrderBy(orderBy).Skip((int)beginIndex).Take((int)pageSize).ToListAsync();

                //
            }
            else if (beginIndex != null)
            {
                dogs = await appDbContext.Dogs.Skip((int)beginIndex).Take((int)pageSize).ToListAsync();
            }
            else if(orderBy!=null)
            {
                //ancaq order by
                dogs= await appDbContext.Dogs.OrderBy(orderBy).ToListAsync();

            }
            else
            {
                dogs= await appDbContext.Dogs.ToListAsync();

            }
            return dogs;

            
            
        }
    }
}
