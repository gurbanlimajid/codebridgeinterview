﻿using InterviewCodeBridge.DAL.Entities;
using InterviewCodeBridge.Model.Request;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InterviewCodeBridge.Repo
{
    public interface IDogRepo
    {
        public Task CheckExistance(DogRequest dog);
        public Task CreateDog(Dog dog);
        public  Task<List<Dog>> GetDogs(string orderBy, int? beginIndex, int? pageSize);
    }
}
