﻿using InterviewCodeBridge.Model.Response;
using InterviewCodeBridge.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InterviewCodeBridge.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PingController : ControllerBase
    {
        [HttpGet]
        public ApiValueResponse<PingResponse> Index()
        {
            PingResponse response = new PingResponse("Dogs house service. Version 1.0.1"); 
            return new ApiValueResponse<PingResponse>(response);

        }
    }
}
