﻿using InterviewCodeBridge.DAL.Entities;
using InterviewCodeBridge.Model.Request;
using InterviewCodeBridge.Model.Response;
using InterviewCodeBridge.Response;
using InterviewCodeBridge.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InterviewCodeBridge.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DogController : ControllerBase
    {
        private readonly IDogService dogService;

        public DogController(IDogService dogService)
        {
            this.dogService = dogService;
        }
        [HttpPost("create")]
        public async Task<ApiResponse> CreateDog(DogRequest dog)
        {
            if (ModelState.IsValid)
            {
                await dogService.CreateDog(dog);

            }
            return new ApiResponse();
            

        }
        [HttpGet("")]
        public async Task<ApiValueResponse<List<DogResponse>>> GetDogs(string attribute, string order, int? pageNumber, int? pageSize)
        {
           var dogResponses =   await dogService.GetDogs(attribute, order, pageNumber, pageSize);   

            return new ApiValueResponse<List<DogResponse>>(dogResponses);

        }
    }
}
