using InterviewCodeBridge.DAL.Context;
using InterviewCodeBridge.Errors;
using InterviewCodeBridge.Model.Request;
using InterviewCodeBridge.Repo;
using InterviewCodeBridge.Service;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System.Threading.Tasks;

namespace InterviewCodeBridge.UnitTest
{
    public class Tests
    {
        private AppDbContext _context;
        private IDogRepo dogRepo;
        private IDogService dogService;

        [SetUp]
        public void Setup()
        {
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseSqlServer("Server=DESKTOP-OK5RUIF;Database=CodeBridge;Trusted_Connection=True;MultipleActiveResultSets=true");
            _context = new AppDbContext(optionsBuilder.Options);
            dogRepo = new DogRepo(_context);
            dogService = new DogService(dogRepo);
        }

        [Test]
        [TestCase("name","asc",null,null)]
        [TestCase("name", "desc", null, null)]
        [TestCase("name", "desc", 1, 5)]
        [TestCase(null, null, 1, 5)]



        public void CheckInputValidation_InputValidation_ReturnTrue(string attribute, string order, int? pageNumber, int? pageSize)
        {
            var result = dogService.CheckInputValidation(attribute, order, pageNumber, pageSize);
            
            //attribute_order_error
            Assert.That(result, Is.EqualTo(true));
        }
        [Test]
        [TestCase("name", null,null,null, "attribute_order_error", "CheckInputValidation")]
        [TestCase(null, "asc", null, null, "attribute_order_error","CheckInputValidation")]
        [TestCase(null, null, 1, null, "number_size_error","CheckInputValidation")]
        [TestCase(null, null, null, 1, "number_size_error","CheckInputValidation")]
        [TestCase(null,null,-5,10, "pagination_not_correct", "CheckInputValidation")]
        [TestCase(null, null, 10, -5, "pagination_not_correct", "CheckInputValidation")]
        [TestCase("sa", "asc", null, null, "attribute_not_correct", "GetDogs")]
        [TestCase("sa", "desc", null, null, "attribute_not_correct", "GetDogs")]
        [TestCase("name", "dsc", null, null, "order_not_correct", "GetDogs")]
        public async Task CheckInputValidation_InputValidation_ReturnCustomError(string attribute, string order, int? pageNumber, int? pageSize,string errorResult,string functionName)
        {
            string errorMessage = "";
            try
            {
                if(functionName== "CheckInputValidation")
                {
                    var result = dogService.CheckInputValidation(attribute, order, pageNumber, pageSize);


                }else if(functionName== "GetDogs")
                {
                    var result = await dogService.GetDogs(attribute, order, pageNumber, pageSize);

                }

            }
            catch (System.Exception ex)
            {
                errorMessage = ex.Message;       
            }

            Assert.That(errorMessage, Is.EqualTo(errorResult));

        }
        [Test]
        [TestCase("dog","dog", 10,-5, "weight_length_error")]
        [TestCase("dog", "dog", -10, 5, "tail_length_error")]

        public async Task CheckCreateDog_CreateDog_ReturnCustomError(string name, string color, int tailLength, int weight,string errorResult)
        {
            DogRequest dogRequest = new DogRequest()
            {
                Name = name,
                Color = color,
                TailLength = tailLength,
                Weight = weight

            };
            string errorMessage = " ";
            try
            {
                 await dogService.CreateDog(dogRequest);

            }
            catch (System.Exception ex)
            {

                errorMessage=ex.Message;
            }
            Assert.That(errorMessage,Is.EqualTo(errorResult));

        }

    }
}